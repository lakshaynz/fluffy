up:
	make down && docker compose up --build -d && make logs
logs:
	docker compose logs -f
down:
	docker compose down
bash:
	docker compose exec fluffy sh
db:
	docker compose exec fluffy sh -c 'python manage.py makemigrations'
	docker compose exec fluffy sh -c 'python manage.py migrate'