# views.py

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import UploadFileForm
from .file_handling import handle_uploaded_file, delete_old_files
import logging

# Initialize the logger
logger = logging.getLogger(__name__)

def main(req):
    ctx = {}
    template = "upload.html"
    if req.method == "POST":
        form = UploadFileForm(req.POST, req.FILES)
        if form.is_valid():
            try:
                mp3_file_path = handle_uploaded_file(req.FILES["file"])
                ctx["text"] = "File uploaded and converted successfully."
                ctx["download_link"] = mp3_file_path  # Add the download link to the context
            except ValueError as ve:
                ctx["text"] = str(ve)  # Display the error message to the user
            except Exception as e:
                ctx["text"] = "An error occurred during file processing."
                logger.error(f"Unexpected error: {e}")
        else:
            ctx["text"] = "Form is not valid."
    else:
        form = UploadFileForm()
    
    ctx["form"] = form
    return render(req, template, ctx)
