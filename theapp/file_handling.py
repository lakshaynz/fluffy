# file_handling.py

import os
import datetime
import ffmpeg
import logging
from django.conf import settings

# Initialize the logger
logger = logging.getLogger(__name__)

# Retrieve directory and delete time settings from Django settings with defaults
USER_UPLOADED_FILES_DIR = getattr(settings, "USER_UPLOADED_FILES_DIR", "/usermedia")
DELETE_OLD_FILES_AFTER_HOURS = getattr(settings, "DELETE_OLD_FILES_AFTER_HOURS", 1)

def write_file_to_disk(f, file_path):
    """Writes uploaded file to disk."""
    with open(file_path, "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def convert_to_mp3(file_path, output_path):
    """Converts an audio file to MP3 format using ffmpeg."""
    try:
        if not file_path or not os.path.exists(file_path):
            raise FileNotFoundError(f"Input file not found: {file_path}")
        if not output_path:
            raise ValueError("Output path is empty")

        # Check if the input file has an audio stream
        probe = ffmpeg.probe(file_path)
        audio_streams = [stream for stream in probe['streams'] if stream['codec_type'] == 'audio']
        
        if not audio_streams:
            raise ValueError("Input file does not contain an audio stream.")

        (
            ffmpeg
            .input(file_path)
            .output(output_path, format='mp3')
            .run(overwrite_output=True)
        )
    except ffmpeg.Error as e:
        error_message = e.stderr.decode() if e.stderr else "No error message available"
        logger.error(f"ffmpeg error: {error_message}")
        raise
    except Exception as e:
        logger.error(f"Error converting to MP3: {e}")
        raise

def delete_old_files():
    """Deletes files in the user-uploaded directory older than a specified time."""
    now = datetime.datetime.now()
    for filename in os.listdir(USER_UPLOADED_FILES_DIR):
        file_path = os.path.join(USER_UPLOADED_FILES_DIR, filename)
        if os.path.isfile(file_path):
            file_time = datetime.datetime.fromtimestamp(os.path.getmtime(file_path))
            age = now - file_time
            if age > datetime.timedelta(hours=DELETE_OLD_FILES_AFTER_HOURS):
                os.remove(file_path)
                logger.info(f"Deleted: {filename} because it was older than {DELETE_OLD_FILES_AFTER_HOURS} hours")

def handle_uploaded_file(f):
    """Handles the uploaded file: saves, converts to MP3, and cleans up old files."""
    # Generate a unique file name to avoid overwriting
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    orig_file_path = os.path.join(USER_UPLOADED_FILES_DIR, f"uploadedfile_{timestamp}")
    mp3_file_path = f"{orig_file_path}.mp3"
    write_file_to_disk(f, orig_file_path)
    convert_to_mp3(orig_file_path, output_path=mp3_file_path)
    os.remove(orig_file_path)

    delete_old_files()

    return mp3_file_path  # Return the path of the converted MP3 file
